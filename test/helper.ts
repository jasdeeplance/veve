import { Nft } from '@modules/nft/nft.entity';
import { User } from '@modules/user/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  DataSource,
  EntityTarget,
  InsertResult,
  ObjectLiteral,
  Repository,
} from 'typeorm';

export type MockType<T> = {
  [P in keyof T]?: jest.Mock<{}>;
};

export const TypeOrmMysqlTestingModule = () => [
  TypeOrmModule.forRoot({
    type: 'mysql',
    url: 'mysql://root:xyzzy264@34.100.230.66:3306/test',
    entities: [User, Nft],
    synchronize: true,
    dropSchema: true,
  }),
];

export async function loadFixtures<T>(
  dataSource: DataSource,
  dataPoints: T[],
  entity: EntityTarget<T>,
): Promise<void> {
  const entityManager = dataSource.createEntityManager();
  await entityManager.save(entity, dataPoints);
}

export const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(
  () => ({
    findOne: jest.fn((entity) => entity),
  }),
);
