import { AuthModule } from '@modules/auth/auth.module';
import { AuthService } from '@modules/auth/services/auth.service';
import { IAuthService } from '@modules/auth/services/IAuth.service';
import { INft, Nft } from '@modules/nft/nft.entity';
import { nfts } from '@modules/nft/seeder/nft.data';
import { users } from '@modules/user/seeder/user.data';
import { IUser, User } from '@modules/user/user.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { loadFixtures, TypeOrmMysqlTestingModule } from '@test/helper';
import { DataSource } from 'typeorm';

describe('AuthService', () => {
  let service: IAuthService;
  let moduleRef: TestingModule;
  let dataSource: DataSource;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [...TypeOrmMysqlTestingModule(), AuthModule],
    }).compile();

    service = moduleRef.get<IAuthService>(AuthService);
    dataSource = moduleRef.get<DataSource>(DataSource);
    await loadFixtures<INft>(dataSource, nfts, Nft);
    await loadFixtures<IUser>(dataSource, users, User);
  });

  afterAll(async () => {
    await moduleRef.close();
  });

  it('can successfully validate user', async () => {
    const user = await service.validateUser('user1@gmail.com', 'test1234');
    expect(user).toMatchSnapshot();
  });

  it('returns null in case of invalid auth', async () => {
    const user = await service.validateUser('user1@gmail.com', 'wrong-pass');
    expect(user).toBeNull();
  });
});
