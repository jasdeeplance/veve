import { NftModel } from '@modules/nft/graphql/models/nft.model';
import { NftPaginatedModel } from '@modules/nft/graphql/models/nft_paginated.model';
import { INft, Nft } from '@modules/nft/nft.entity';
import { NftModule } from '@modules/nft/nft.module';
import { nfts } from '@modules/nft/seeder/nft.data';
import { INftService } from '@modules/nft/services/INft.service';
import { NftService } from '@modules/nft/services/nft.service';
import { users } from '@modules/user/seeder/user.data';
import { IUser, User } from '@modules/user/user.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { loadFixtures, TypeOrmMysqlTestingModule } from '@test/helper';
import { DataSource } from 'typeorm';

describe('NftService', () => {
  let service: INftService;
  let moduleRef: TestingModule;
  let dataSource: DataSource;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [...TypeOrmMysqlTestingModule(), NftModule],
    }).compile();

    service = moduleRef.get<INftService>(NftService);
    dataSource = moduleRef.get<DataSource>(DataSource);
    await loadFixtures<INft>(dataSource, nfts, Nft);
    await loadFixtures<IUser>(dataSource, users, User);
  });

  afterAll(async () => {
    await moduleRef.close();
  });

  it('can get nft by id', async () => {
    const nft = await service.getNftById(1);
    expect(nft.id).toBe(1);
  });

  it('can get paginated nft', async () => {
    // get 1 nft for 0 offset for user id 1
    const res: NftPaginatedModel = await service.getAll(1, 1, 0);
    const { edges: nfts } = res;
    const sanitizedNfts: Array<NftModel> = nfts.map((nft: Nft) => {
      delete nft.created_at;
      delete nft.updated_at;
      delete nft.mintDate;
      return nft;
    });
    expect(sanitizedNfts).toMatchSnapshot();
  });

  it('can transfer nft', async () => {
    // send nft id 1 from user id 1 to user id 2
    await service.transferNft(1, 1, 2);
    const nft = await service.getNftById(1);
    expect(nft.user.id).toBe(2);
  });
});
