import { Module } from '@nestjs/common';
import { UserModule } from '@modules/user/user.module';
import { MysqlProviderModule } from './providers/mysql.provider';
import { MySQLConfigModule } from '@config/mysql/mysql.module';
import { AuthModule } from '@modules/auth/auth.module';
import { GraphqlProviderModule } from './providers/graphql.provider';
import { NftModule } from '@modules/nft/nft.module';

@Module({
  imports: [
    GraphqlProviderModule,
    MySQLConfigModule,
    MysqlProviderModule,
    UserModule,
    AuthModule,
    NftModule,
  ],
  providers: [],
})
export class AppModule {}
