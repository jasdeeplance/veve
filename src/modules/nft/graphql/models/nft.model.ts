import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class NftModel {
  @Field()
  name: string;

  @Field()
  blockchainLink: string;

  @Field()
  description: string;

  @Field()
  imageUrl: string;

  @Field()
  mintDate: Date;
}
