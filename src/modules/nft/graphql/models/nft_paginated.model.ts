import { Field, Int, ObjectType } from '@nestjs/graphql';
import { NftModel } from './nft.model';

@ObjectType()
export class NftPaginatedModel {
  @Field(() => [NftModel])
  edges: NftModel[];

  @Field(() => Int)
  totalCount: number;
}
