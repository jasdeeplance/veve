import { Field, ArgsType, Int } from '@nestjs/graphql';

@ArgsType()
export class NftTransferArgs {
  @Field(() => Int)
  receiver: number;

  @Field(() => Int)
  id: number;
}
