import { ArgsType, Field, Int } from '@nestjs/graphql';

@ArgsType()
export class NftPaginatedArgs {
  @Field(() => Int)
  offset: number;

  @Field(() => Int)
  size: number;
}
