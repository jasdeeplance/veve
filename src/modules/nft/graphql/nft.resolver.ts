import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { NftTransferArgs } from './dtos/nft_transfer.dto';
import { NftService } from '../services/nft.service';
import { NftModel } from './models/nft.model';
import { NftPaginatedModel } from './models/nft_paginated.model';
import { NftPaginatedArgs } from './dtos/nft_paginated.dto';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '@modules/auth/guards/auth.jwt-guard';
import { RolesGuard } from '@modules/auth/guards/auth.role-guard';
import { Role } from '@modules/user/types';
import { Roles } from '@modules/auth/decorators/auth.roles-decorator';
import { AuthInfo } from '@modules/auth/types';
import { CurrentUser } from '@modules/auth/decorators/auth.user-decorator';

@Resolver(() => NftModel)
export class NftResolver {
  constructor(private nftService: NftService) {}

  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles([Role.User, Role.Admin])
  @Query(() => NftPaginatedModel)
  async nfts(
    @CurrentUser() info: AuthInfo,
    @Args() args: NftPaginatedArgs,
  ): Promise<NftPaginatedModel> {
    const { offset, size } = args;
    const { userId } = info;
    const paginatedNfts = await this.nftService.getAll(userId, size, offset);
    return paginatedNfts;
  }

  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles([Role.User, Role.Admin])
  @Mutation(() => NftModel)
  async transfer(
    @CurrentUser() info: AuthInfo,
    @Args() args: NftTransferArgs,
  ): Promise<NftModel> {
    const { userId } = info;
    const { id, receiver } = args;
    const nft = await this.nftService.transferNft(id, userId, receiver);
    return nft;
  }
}
