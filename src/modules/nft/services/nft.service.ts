import { Injectable, Logger, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { INft, Nft } from '@modules/nft/nft.entity';
import { NftPaginatedModel } from '../graphql/models/nft_paginated.model';
import { INftService } from './INft.service';
import { ForbiddenError } from '@nestjs/apollo';
import { nfts } from '../seeder/nft.data';
import { UserService } from '@modules/user/services/user.service';

@Injectable()
export class NftService implements INftService {
  private readonly logger = new Logger(UserService.name);

  constructor(
    private userService: UserService,
    @InjectRepository(Nft)
    private nftRepository: Repository<Nft>,
  ) {}

  async getAll(
    userId: number,
    take: number,
    skip: number,
  ): Promise<NftPaginatedModel> {
    this.logger.log(`fetching nfts for user: ${userId} at offset ${skip}`);
    const [edges, totalCount] = await this.nftRepository.findAndCount({
      where: { user: { id: userId } },
      take,
      skip,
    });
    return {
      edges,
      totalCount,
    };
  }

  async transferNft(
    id: number,
    senderId: number,
    receiverId: number,
  ): Promise<INft> {
    const nft = await this.getNftById(id);
    const receiverUser = await this.userService.getUserById(receiverId);
    if (!this.validateOwnership(nft, senderId)) {
      throw new ForbiddenError('validation of ownership failed');
    }
    nft.user = receiverUser;
    await this.nftRepository.save(nft);
    this.logger.log(`transferring nft with id ${nft.id} to user ${receiverId}`);
    return nft;
  }

  async getNftById(id: number): Promise<Nft> {
    const nft = await this.nftRepository.findOneBy({ id });
    return nft;
  }

  async seed(): Promise<void> {
    const promises: Array<Promise<INft>> = [];
    nfts.map((nft: INft) => {
      promises.push(this.nftRepository.save(nft));
    });
    await Promise.all(promises);
  }

  private validateOwnership(nft: INft, userId: number): boolean {
    if (!nft.user) {
      return false;
    }
    return nft.user!.id === userId;
  }
}
