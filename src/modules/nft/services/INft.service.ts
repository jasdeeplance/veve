import { NftPaginatedModel } from '../graphql/models/nft_paginated.model';
import { INft } from '../nft.entity';

export interface INftService {
  getAll(owner: number, take: number, skip: number): Promise<NftPaginatedModel>;
  transferNft(id: number, senderId: number, receiverId: number): Promise<INft>;
  getNftById(id: number): Promise<INft>;
}
