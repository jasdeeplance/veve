import { IUser, User } from '@modules/user/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

export interface INft {
  id: number;
  name: string;
  blockchainLink: string;
  description: string;
  imageUrl: string;
  mintDate: Date;
  user?: IUser;
}

@Entity()
export class Nft implements INft {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  blockchainLink: string;

  @Column()
  description: string;

  @Column()
  imageUrl: string;

  @Column()
  mintDate: Date;

  @ManyToOne(() => User, (user) => user.nfts, { nullable: true, eager: true })
  user?: IUser;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
