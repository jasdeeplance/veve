import { UserModule } from '@modules/user/user.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NftResolver } from './graphql/nft.resolver';
import { Nft } from './nft.entity';
import { NftService } from './services/nft.service';

@Module({
  imports: [TypeOrmModule.forFeature([Nft]), UserModule],
  providers: [NftService, NftResolver],
})
export class NftModule {}
