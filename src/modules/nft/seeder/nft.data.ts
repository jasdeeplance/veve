import { INft } from '../nft.entity';

export const nfts: INft[] = [
  {
    id: 1,
    name: 'nft1',
    blockchainLink: 'https://some1.com',
    description: 'description',
    imageUrl: 'https://some.com',
    mintDate: new Date(),
  },
  {
    id: 2,
    name: 'nft2',
    blockchainLink: 'https://some2.com',
    description: 'description',
    imageUrl: 'https://some.com',
    mintDate: new Date(),
  },
];
