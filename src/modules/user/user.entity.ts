import { INft, Nft } from '@modules/nft/nft.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Role } from './types';

export interface IUser {
  id: number;
  name: string;
  email: string;
  password: string;
  roles: Role[];
  nfts?: INft[];
}

@Entity()
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column('simple-array')
  roles: Role[];

  @OneToMany(() => Nft, (nft) => nft.user, { nullable: true })
  nfts?: INft[];
}
