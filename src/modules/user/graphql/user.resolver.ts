import { Roles } from '@modules/auth/decorators/auth.roles-decorator';
import { CurrentUser } from '@modules/auth/decorators/auth.user-decorator';
import { GqlAuthGuard } from '@modules/auth/guards/auth.jwt-guard';
import { RolesGuard } from '@modules/auth/guards/auth.role-guard';
import { AuthInfo } from '@modules/auth/types';
import { UseGuards } from '@nestjs/common';
import { Query, Resolver } from '@nestjs/graphql';
import { UserService } from '../services/user.service';
import { Role } from '../types';
import { UserModel } from './models/user.model';

@Resolver(() => UserModel)
export class UserResolver {
  constructor(private userService: UserService) {}

  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles([Role.User])
  @Query(() => UserModel)
  async user(@CurrentUser() info: AuthInfo): Promise<UserModel> {
    const { userId } = info;
    const user = await this.userService.getUserById(userId);
    return user;
  }
}
