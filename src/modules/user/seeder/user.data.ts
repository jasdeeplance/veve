import { nfts } from '@modules/nft/seeder/nft.data';
import { IUser } from '@modules/user/user.entity';
import { Role } from '../types';

// test1234 -> 2bea196e3e717506d94909d109601ab33313317b3118fa2392e4a9f273d3ac7f
export const users: IUser[] = [
  {
    id: 1,
    name: 'user1',
    email: 'user1@gmail.com',
    password:
      '2bea196e3e717506d94909d109601ab33313317b3118fa2392e4a9f273d3ac7f',
    roles: [Role.User],
    nfts: [nfts[0]],
  },
  {
    id: 2,
    name: 'user2',
    email: 'user2@gmail.com',
    password:
      '2bea196e3e717506d94909d109601ab33313317b3118fa2392e4a9f273d3ac7f',
    roles: [Role.Admin],
    nfts: [nfts[1]],
  },
];
