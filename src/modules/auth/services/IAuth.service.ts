import { IUser } from '@modules/user/user.entity';

export interface IAuthService {
  validateUser(email: string, pass: string): Promise<IUser | null>;
  createSession(user: IUser): string;
}
