import { UserService } from '@modules/user/services/user.service';
import { Injectable } from '@nestjs/common';
import { createHmac } from 'crypto';
import { JwtService } from '@nestjs/jwt';
import { IAuthService } from './IAuth.service';
import { AuthInfo } from '../types';
import { IUser } from '@modules/user/user.entity';

@Injectable()
export class AuthService implements IAuthService {
  private static readonly HASH_KEY = 'test';

  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<IUser | null> {
    const user = await this.userService.findUserByEmail(email);
    const password = this.hashString(pass);
    if (user && user.password === password) {
      return user;
    }
    return null;
  }

  createSession(user: IUser): string {
    const payload: AuthInfo = { userId: user.id, roles: user.roles };
    const token = this.jwtService.sign(payload);
    return token;
  }

  // should be it's own separate service or a util.
  private hashString(str: string): string {
    return createHmac('sha256', AuthService.HASH_KEY)
      .update(str.trim())
      .digest('hex');
  }
}
