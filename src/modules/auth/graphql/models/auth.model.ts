import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AuthModel {
  @Field()
  token: string;
}
