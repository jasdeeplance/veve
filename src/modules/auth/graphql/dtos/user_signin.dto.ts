import { ArgsType, Field, Int } from '@nestjs/graphql';

@ArgsType()
export class UserSigninArgs {
  @Field()
  email: string;

  @Field()
  password: string;
}
