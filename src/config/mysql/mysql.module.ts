import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import configuration from './mysql.env';
import { MysqlConfigService } from './mysql.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
/**
 * Import and provide mysql configuration related classes.
 *
 * @module
 */
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        MYSQL_HOST: Joi.string().default('34.100.230.66'),
        MYSQL_PORT: Joi.number().default(3306),
        MYSQL_USERNAME: Joi.string().default('root'),
        MYSQL_PASSWORD: Joi.string().default('xyzzy264'),
        MYSQL_DATABASE: Joi.string().default('veve'),
      }),
    }),
  ],
  providers: [ConfigService, MysqlConfigService],
  exports: [ConfigService, MysqlConfigService],
})
export class MySQLConfigModule {}
