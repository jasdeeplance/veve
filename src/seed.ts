import { NftService } from '@modules/nft/services/nft.service';
import { UserService } from '@modules/user/services/user.service';
import { INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const appContext: INestApplicationContext = await NestFactory.createApplicationContext(
    AppModule,
  );

  const userSeeder = appContext.get(UserService);
  const nftSeeder = appContext.get(NftService);

  try {
    await nftSeeder.seed();
    await userSeeder.seed();
  } finally {
    appContext.close();
  }
}

bootstrap()
  .then(() => console.log('seeding done'))
  .catch((e) => console.log('seeding failed', e));
