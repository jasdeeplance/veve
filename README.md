# Veve

This is the basic code for take home assignment by Veve.

## Setup

To run simply do

```bash
  docker-compose up
```

## Tech Stack

Nest, Typeorm, mysql, docker

## Decisions

- This project is structured by folder-by-feature. My rationale and opinion on it can be found [here](https://softwareengineering.stackexchange.com/a/338610)
- It was a little hard to gather the convention for filename used by nest. I ended up using convention `feature.domain.ts`
- Graphql schema generation is code first. This is something which I prefer.
- Unit tests use real db. Technically they aren't pure unit tests but rather start tilting towards integration tests. Reason can be found [here](https://stackoverflow.com/a/310336). Essentially, since I also wanted to check data access integration therefore it made sense to use real db. Given more time I would like to more tests especially around JWT auth. Both integration as well as unit.
- This project uses no error abstractions. But rather relies on errors provided by nest. This is because of time constraints.
- There are values which are either in constants or being derived from env. They should actually be coming from some sort of secret manager. For example: salt, jwtsecret etc. Given more time, I would integrate a secret manager get values from it.
- Dependencies have been kept to a minimal though at certain places I choose to have some dependencies to make code more extensible. For example: validation of env values.
- While the code does have dependency injection, it does not have dependency inversion. This is because this was my first time with nest and due to time constraints I could not figure out how to rely on abstractions in providers. Given more time, I would like to know how to do that to achieve inversion as well.
- In user module, email is unique and should be indexed. But I kept geting `QueryFailedError: Duplicate key name ‘IDX_e12875dfb3b1d92d7d7c5377e2’`. And to improve test coverage I let go of indexing and this error. Though this is something which I would like to fix.
- `eager` loading is set to true in relation of nft and user. That is not a great idea. But since this code is for testing we can let it be now. Though ideally it should be false and a query should be build to get values of those relations.
- I would've liked to write pure integration tests for resolvers. But due to time constraints couldn't. Therefore guard, passport and gql context code is untested.
- Note: There is an in built seed command in dockerfile which uploads test data to mysql for easier playing around. Though since we do volume binding the data is persistent in subsequent runs. So seed would actually update the data if it already exists. So you might see same base data in all runs.
- Typescript config right now sucks. It allows unknowns, any and unused variables. Due to time constraints I couldn't update that. Given more time I would make typescript config more robust.
- Note on directives: one of the requirements of the project was: `only expose the following fields to the client`. Here it is not specified who is the client. It may be only frontend or it may be any consumer. In case it is a client, then a simple skip of the fields which ought not be exposed would work which is the approach taken here(they're omitted from graphql schema. I could also write a directive but given the time constraints this was the KISS approach).
- I use the default logger which comes with nest since it would get the job done. In a production scenario it is preferred to use custom logger with some way to transport logs.

## Other info

- node version: 16LTS
- time taken: 6:30 hours
